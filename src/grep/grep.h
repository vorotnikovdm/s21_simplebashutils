typedef struct {
  int e;
  int echeck;
  int i;
  int v;
  int c;
  int l;
  int n;
  int h;
  int s;
} Flags;
void openfile(int argc, char *filename, int index, int *flags,
              int counttemplates, char **templates, int countfiles);
void applyflags(int argc, char *filename, int index, int const *flags,
                int counttemplates, char **templates, int countfiles);
int getregex(char *template, char *line, Flags flags);
void linesprinting(char *line, char *filename, int argc, int index,
                   int rowscount, int lcheck, Flags flags);
void otherprinting(char *filename, int rows, int vrows, int countfiles,
                   Flags flags);
void free_templates(char **templates, int counttemplates);
Flags parseFlags(const int *flags, int counttemplates);
