#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "grep.h"

int main(int argc, char **argv) {
  int flags[8] = {0};
  int ecount = 0, option_index = 0, counttemplates = 0, countfiles = 0;
  char **templates = NULL;
  static struct option long_options[] = {{0, 0, 0, 0}};
  for (int i = 0; i < argc; i++) {
    flags[i] =
        getopt_long(argc, argv, "e:ivclnhs", long_options, &option_index);
    if (flags[i] == 'e') {
      counttemplates++;
      templates = realloc(templates, counttemplates * sizeof(char *));
      if (templates == NULL) {
        free(templates);
        printf("Memory allocation failed\n");
        exit(EXIT_FAILURE);
      } else {
        templates[counttemplates - 1] =
            malloc((strlen(optarg) + 1) * sizeof(char));
        if (templates[counttemplates - 1] == NULL) {
          free(templates);
          printf("Memory allocation failed\n");
          exit(EXIT_FAILURE);
        } else
          strcpy(templates[counttemplates - 1], optarg);
      }
    }
    for (int j = 0; j < i; j++) {
      if (flags[i] == flags[j]) i--;
    }
    if (flags[i] == -1) break;
  }
  for (int w = 0; w < 8; w++) {
    if (flags[w] == 'e') {
      ecount++;
      break;
    }
  }
  if (ecount == 0) {
    counttemplates++;
    templates = malloc(counttemplates * sizeof(char *));
    if (templates == NULL)
      free_templates(templates, counttemplates);
    else {
      templates[counttemplates - 1] =
          malloc((strlen(argv[optind]) + 1) * sizeof(char));
      strcpy(templates[counttemplates - 1], argv[optind]);
    }
    for (int j = optind + 1; j < argc; j++) {
      countfiles = argc - 1 - optind;
      openfile(argc, argv[j], optind, flags, counttemplates, templates,
               countfiles);
    }
  } else
    for (int j = optind; j < argc; j++) {
      countfiles = argc - optind;
      openfile(argc, argv[j], optind, flags, counttemplates, templates,
               countfiles);
    }
  free_templates(templates, counttemplates);
}
