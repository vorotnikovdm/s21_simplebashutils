#include "grep.h"

#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void openfile(int argc, char *filename, int index, int *flags,
              int counttemplates, char **templates, int countfiles) {
  int s = 0;
  for (int i = 0; i < 8; i++)
    if (flags[i] == 's') s = 1;
  FILE *fp;
  if ((fp = fopen(filename, "r")) != NULL) {
    applyflags(argc, filename, index, flags, counttemplates, templates,
               countfiles);
    fclose(fp);
  } else {
    if (s == 0)
      fprintf(stderr, "grep: %s: No such file or directory\n", filename);
  }
}

void applyflags(int argc, char *filename, int index, int const *flags,
                int counttemplates, char **templates, int countfiles) {
  FILE *fp;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  int lcheck = 0, vrows = 0, rows = 0, rowscount = 0, regexcheck = 0;
  Flags parsedFlags = parseFlags(flags, counttemplates);
  fp = fopen(filename, "r");
  while ((read = getline(&line, &len, fp)) != -1) {
    int count = 0;
    rowscount++;
    for (int j = 0; j < counttemplates; j++) {
      regexcheck = getregex(templates[j], line, parsedFlags);
      if (regexcheck == 7) {
        free_templates(templates, counttemplates);
        fclose(fp);
        free(line);
        exit(1);
      } else if (regexcheck == 1) {
        if ((parsedFlags.v == 1) && (counttemplates > 1)) {
          count++;
          continue;
        }
        linesprinting(line, filename, argc, index, rowscount, lcheck,
                      parsedFlags);
        lcheck++;
        if (parsedFlags.l == 1) rows++;
        if (parsedFlags.c == 1) {
          if ((templates[j][0] == '.') && (line[0] == '\n'))
            continue;
          else
            rows++;
        }
        if ((parsedFlags.l == 1) && (parsedFlags.v == 1)) vrows++;
        break;
      }
    }
    if ((parsedFlags.v == 1) && (counttemplates > 1) &&
        (count == counttemplates)) {
      linesprinting(line, filename, argc, index, rowscount, lcheck,
                    parsedFlags);
      lcheck++;
      if ((parsedFlags.c == 1) && (parsedFlags.l == 0)) rows++;
      if (parsedFlags.l == 1) vrows++;
    }
  }
  otherprinting(filename, rows, vrows, countfiles, parsedFlags);
  fclose(fp);
  free(line);
}

int getregex(char *template, char *line, Flags flags) {
  int check = 0;
  regex_t regex;
  if (flags.i == 0) {
    if (regcomp(&regex, template, REG_EXTENDED) != 0) {
      printf("Regex compile failed\n");
      check = 7;
    }
  } else {
    if (regcomp(&regex, template, REG_EXTENDED | REG_ICASE) != 0) {
      check = 7;
    }
  }
  if (check == 0)
    if (((flags.e == 1) && (flags.v == 0) &&
         (regexec(&regex, line, 0, 0, 0) == 0)) ||
        ((flags.e == 1) & (flags.v == 1) &&
         (regexec(&regex, line, 0, 0, 0) != 0)))
      check = 1;
  regfree(&regex);
  return check;
}

void linesprinting(char *line, char *filename, int argc, int index,
                   int rowscount, int lcheck, Flags flags) {
  int check;
  if (flags.echeck == 0)
    check = argc - 1;
  else
    check = argc - 2;
  if ((check != index) && (flags.l == 0) && (flags.c == 0) && (flags.h == 0))
    printf("%s:", filename);
  else if (((flags.c == 1) && (flags.l == 1) && (check != index)) ||
           ((check != index) && (flags.c == 1)))
    if ((lcheck == 0) && (flags.h == 0) && (flags.l == 0))
      printf("%s:", filename);
  if ((flags.c == 0) && (flags.l == 0) && (flags.n == 0)) {
    printf("%s", line);
    if ((line[strlen(line) - 1]) != '\n') printf("\n");
  } else if ((flags.n == 1) && (flags.c == 0) && (flags.l == 0)) {
    printf("%d:%s", rowscount, line);
    if ((line[strlen(line) - 1]) != '\n') printf("\n");
  }
}

void otherprinting(char *filename, int rows, int vrows, int countfiles,
                   Flags flags) {
  if ((flags.c == 1) && (flags.l == 0) && (rows != 0)) printf("%d\n", rows);
  if ((flags.v == 0) && (flags.h == 0)) {
    if ((flags.c == 1) && (flags.l == 1)) {
      if ((countfiles > 1) && (rows == 0))
        printf("%s:%d\n", filename, rows);
      else if ((countfiles > 1) && (rows > 1))
        printf("%s:1\n%s\n", filename, filename);
      else
        printf("1\n%s\n", filename);
    }
  } else {
    if ((flags.c == 1) && (flags.l == 1) && (flags.h == 0)) {
      if ((countfiles > 1) && (vrows == 0))
        printf("%s:%d\n", filename, vrows);
      else if ((countfiles > 1) && (vrows > 1))
        printf("%s:1\n%s\n", filename, filename);
      else if ((countfiles == 1) && (vrows == 0))
        printf("%d\n", vrows);
      else
        printf("1\n%s\n", filename);
    }
  }
  if ((flags.v == 0) && (flags.h == 1)) {
    if ((flags.c == 1) && (flags.l == 1)) {
      if ((countfiles > 1) && (rows == 0))
        printf("%d\n", rows);
      else
        printf("1\n%s\n", filename);
    }
  } else {
    if ((flags.c == 1) && (flags.l == 1) && (flags.h == 1)) {
      if ((countfiles > 1) && (vrows == 0))
        printf("%s:%d\n", filename, vrows);
      else if ((countfiles > 1) && (vrows > 1) && (flags.h == 0))
        printf("%s:1\n%s\n", filename, filename);
      else if ((countfiles == 1) && (vrows == 0))
        printf("%d\n", vrows);
      else
        printf("1\n%s\n", filename);
    }
  }
  if ((flags.c == 1) && (flags.l == 0) && (rows == 0)) {
    if ((countfiles > 1) && (flags.h == 0))
      printf("%s:%d\n", filename, rows);
    else
      printf("%d\n", rows);
  }
  if ((flags.l == 1) && (flags.c == 0) && (flags.v == 0) && (rows > 0))
    printf("%s\n", filename);
  if ((flags.l == 1) && (flags.v == 1) && (vrows > 0) && (flags.c == 0))
    printf("%s\n", filename);
}

void free_templates(char **templates, int counttemplates) {
  for (int i = 0; i < counttemplates; i++) {
    free(templates[i]);
  }
  free(templates);
}

Flags parseFlags(const int *flags, int counttemplates) {
  Flags result = {0};
  for (int q = 0; q < 8; q++) {
    if (flags[q] == 'e') result.e = 1;
    if (flags[q] == 'i') result.i = 1;
    if (flags[q] == 'v') result.v = 1;
    if (flags[q] == 'c') result.c = 1;
    if (flags[q] == 'l') result.l = 1;
    if (flags[q] == 'n') result.n = 1;
    if (flags[q] == 'h') result.h = 1;
  }
  if ((result.e == 0) && (counttemplates > 0)) {
    result.e = 1;
    result.echeck = 1;
  }
  return result;
}
