#include <getopt.h>
#include <stdio.h>
#include <string.h>

#include "cat.h"

int main(int argc, char **argv) {
  int flags[8] = {0};
  int option_index = 0;
  static struct option long_options[] = {{"number-nonblank", 0, 0, 'b'},
                                         {"number", 0, 0, 'n'},
                                         {"squeeze-blank", 0, 0, 's'},
                                         {0, 0, 0, 0}};
  for (int i = 0; i < argc; i++) {
    flags[i] = getopt_long(argc, argv, "benstvET", long_options, &option_index);
    if (flags[i] == -1) break;
    for (int w = 0; w < i; w++) {
      if (flags[i] == flags[w]) i--;
    }
  }
  for (int j = optind; j < argc; j++) {
    openfile(argv[j], flags);
  }
  return 0;
}
